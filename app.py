from flask import Flask
from view import home

def create_app():
    app = Flask(__name__)

    app.secret_key = b"alisecretkey*0-!"

    app.register_blueprint(home.bp)

    return app

app=create_app()